## A simple blog template

Helo, this is based on Hakyll-4.12 (https://github.com/jaspervdj/hakyll)

#### Added features:

- Tags
- Teaser
- Pagination
- 404

#### Usage:

    stack build

    stack exec site clean
    stack exec site build
    stack exec site watch

Note:

- In Stack.yaml, keep `resolver: lts-11.9`
  This prevents `haddock-library...` error

#### Publish on GitPages

    git stash

    git checkout develop // make change
    stack exec site clean
    stack exec site build

    git fetch --all
    git add/commit/push develop
    git checkout -b production --track origin/production

    git checkout develop _site
    cp -afr _site/. .
    git rm -rf _site

    git add --all
    git commit -m "..."
    git push origin production:production

    git checkout develop
    git branch -D production

    git stash pop

Note:

- For Github Pages, replace the branch name `production` with `master`.
