$(document).ready( function () {

	$(".moretagposts li").slice(0, 7).show();
	if ($(".moretagposts li:hidden").length == 0) {
		//$("#loadMore").fadeOut('slow');
		$("#loadMore").css('display', 'none');
	}

	$("#loadMore").on('click', function (e) {
		e.preventDefault();
		$(".moretagposts li:hidden").slice(0, 4).slideDown();
		if ($(".moretagposts li:hidden").length == 0) {
			$(this).fadeOut('slow');

		}
		$('html,body').animate({
			scrollTop: $(this).offset().top
		}, 1500);
	});

});