---
title: Upload Stack executable to EC2
published: 2018-04-21
author: Woodstock
tags: aws, bash
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

<pre><code>
#!/usr/bin/env sh
TEMP=$(echo $(stack exec which $1))
scp -i my.pem $TEMP user@my_aws_ip:/home/user/

</code></pre>

<!-- more -->
