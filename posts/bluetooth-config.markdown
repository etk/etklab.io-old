---
title: Bluetooth Configuration & Usage
published: 2017-10-10
author: Woodstock
tags: gentoo, other linux
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Be part of the `plugdev` group<br>
_If I am a VBox guest:_<br>
manually turn on the device: Device > USB > avaiable bt dongle<br>
make sure the `bluetooth` service is on

<!-- more -->

[Perfect Gentoo Wiki](https://wiki.gentoo.org/wiki/Bluetooth)<br>
[Bluez intro](https://www.pcsuggest.com/linux-bluetooth-setup-hcitool-bluez/)

<pre><code>
emerge --ask bluez alsa extra-tools obex experimental readline test-programs udev 
emerge --ask bluez-firmware rfkill
emerge --ask blueman

</code></pre>

_Note:_<br>
`blueman` is at: Setting > Bluetooth Manager <br>
`hciconfig -a` only behaves when the bluetooth service is running (and is not occupied by different machine)