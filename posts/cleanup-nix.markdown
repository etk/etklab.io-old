---
title: Clean up Nixos
published: 2018-03-17
author: Charlie
tags: nixos
description: Pellentesque justo pharetra commodo mollis, sem magna consequat arcu, sed pretium ipsum arcu sit amet neque condimentum non dui vehicula convallis, nisl eget suspendisse pharetra ullamcorper sem et auctor.
---

Remove old configurations<br>
Collect the garbage

<pre><code>
nix-env -p /nix/var/nix/profiles/system --delete-generations old
nix-collect-garbage -d
nix-env -p /nix/var/nix/profiles/system --list-generations

</code></pre>

<!-- more -->


