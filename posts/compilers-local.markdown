---
title: C Compilers Local Installation
published: 2016-10-24
author: Ovidius
tags: other linux
description: Suspendisse pharetra ullamcorper sem et auctor. Suspendisse vitae tellus eu turpis dignissim gravida ut ut tortor.
---

Clang | GCC | Gold

<!-- more -->

#### Clang

[prerequisites](http://llvm.org/docs/GettingStarted.html#requirements)<br>
Need SVN, cmake >=3.4.3<br>
Do 2>3>4>5 from [instructions](http://clang.llvm.org/get_started.html)<br>
Do 7, with [more options](http://llvm.org/docs/CMake.html)

<pre><code>
cmake -G "Unix Makefiles" \
    -D LLVM_BINUTILS_INCDIR=/your/binutils-X.XX/include \ // where plugin-api.h is
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/your/clang \
    ../llvm
make && make install

</code></pre>

LD_LIBRARY_PATH=/your/clang/lib:$LD_LIBRART_PATH<br>
PATH=/your/clang/bin:$PATH

#### GCC

[prerequisites](https://gcc.gnu.org/install/prerequisites.html)<br>
[Mirror](https://mirror.csclub.uwaterloo.ca/)<br>
Build GMP > MPFR > MPC > GCC

<pre><code>
./configure --prefix=/your/gmp
make -jN && make install
./configure --prefix=/your/mpfr --with-gmp=/your/gmp
make -jN && make install
./configure --prefix=/your/mpc --with-gmp=/your/gmp --with-mpfr=/your/mpfr
make -jN && make install

</code></pre>

LD_LIBRARY_PATH=/your/gmp/lib:/your/mpfr/lib:/your/mpc/lib:$LD_LIBRART_PATH

<pre><code>
./configure --prefix=/your/gcc \
    --with-gmp=/your/gmp --with-mpfr=/your/mpfr --with-mpc=/your/mpc \
    --disable-multilib

</code></pre>

LD_LIBRARY_PATH=/your/gcc/lib64:$LD_LIBRART_PATH<br>
PATH=/your/gcc/bin:$PATH

#### Gold

It's a LTO support via plugins.<br>
Either as root or non-root, [build it](http://llvm.org/docs/GoldPlugin.html), from [DOWNLOADED-SRC](https://ftp.gnu.org/gnu/binutils/)<br>
<br>
Build `build/gold/ld-new` 

<pre><code>
cd /DOWNLOADED-SRC
mkdir build && cd build
../DOWNLOADED-SRC/configure --enable-gold --enable-plugins --disable-werror
make all-gold
gold/ld-new -v // to check

</code></pre>

GNU gold (yey)

Build `build/binutils/ar` and `build/binutils/nm-new`

<pre><code>
make

</code></pre>

##### Link new gold to /

Assume `/usr/bin/ld`, `/usr/bin/ar`, `/usr/bin/nm` exist

<pre><code>
su
cd /usr/bin

mv ld ld-backup
ln -s /DOWNLOADED-SRC/build/gold/ld-new ld

mv ar ar-backup
ln -s /DOWNLOADED-SRC/build/binutils/ar ar

mv nm nm-backup
ln -s /DOWNLOADED-SRC/build/binutils/nm-new nm

exit
ld -v // to check

</code></pre>

GNU gold (yey)


Note: <br>
For a root-level Clang installation (using emerge), see [this link](https://wiki.gentoo.org/wiki/Clang), 
which tells how to setup environment nicely too

#### Hello test with gold

Run this [example](http://llvm.org/docs/LinkTimeOptimization.html#liblto-example)<br>
With GCC

<pre><code>
gcc -flto -c a.c -o a.o
gcc -c main.c -o main.o
gcc -flto a.o main.o -o main

</code></pre>

With Clang

<pre><code>
clang -flto -c a.c -o a.o
clang -c main.c -o main.o
clang -flto -fuse-ld=gold a.o main.o -o main

</code></pre>

Both return 42 :)

#### Boost

<pre><code>
./bootstrap.sh --prefix=

</code></pre>

If you want to build the mpi library, add in `project-config.jam`<br>
using mpi ;
  
<pre><code>  
./b2 --ignore-site-config \
    variant=release \
    threading=multi \
    link=shared \
    runtime-link=shared \
    install 

</code></pre>

`--ignore-site-config` resolves the "not known value of feature" error
