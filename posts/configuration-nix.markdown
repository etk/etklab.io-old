---
title: My Configuration.nix
published: 2018-03-15
author: Charlie
tags: nixos
description: Pellentesque justo pharetra commodo mollis, sem magna consequat arcu, sed pretium ipsum arcu sit amet neque condimentum non dui vehicula convallis, nisl eget suspendisse pharetra ullamcorper sem et auctor.
---

<!-- more -->

<pre><code>
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot = {

    loader.grub.enable = true;
    loader.grub.version = 2;
    # loader.grub.efiSupport = true;
    # loader.grub.efiInstallAsRemovable = true;
    # loader.efi.efiSysMountPoint = "/boot/efi";
    # Define on which hard drive you want to install Grub.
    loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  time.timeZone = "US/Pacific";
  time.hardwareClockInLocalTime = false;
  hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;
  powerManagement.enable = true;
  sound.enable = true;

  networking = {
    hostName = "---";
    # wireless.enable = true; use networkmanager instead
    networkmanager.enable = true;
    firewall.enable = true;
    firewall.allowPing = false;
    firewall.allowedTCPPorts = [ -- -- -- ];
    firewall.allowedUDPPorts = [ -- -- -- ];
  };

  environment.systemPackages = with pkgs; [
    networkmanagerapplet	dpkg	gparted	nixops
    gnupg	qbittorrent
    bluez	blueman 
    pa_applet	htop	atop	iotop	strace	lsof 
    curl	wget	nmap	telnet	vsftpd	ssmtp	wireshark tcpdump	netcat-openbsd 	nginx
    p7zip	unzip 		unrar
    bash	zsh	vim
    libevent	bison	ctags	cron	binutils	file
    arandr
    xfce.thunar
    xfce.xfce4notifyd
    xfce.xfce4_power_manager
    xfce.xfce4-hardware-monitor-plugin
    xfce.xfce4-screenshooter
    xfce.xfce4volumed
    xfce.xfce4settings
    xfce.xfconf
 ];

  nixpkgs.config = {
    allowUnfree = true;
    virtualbox.enableExtensionPack = true;
  };

 fonts.fonts = with pkgs; [
    inconsolata terminus_font ubuntu_font_family
 ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  fileSystems = [
    {
      device = "/dev/sda3";
      #mountPoint = "/mnt";
    }

    {
      device = "/dev/sda1";
      mountPoint = "/storage";
      options = [ "rw" ];
    }
  ];

  swapDevices = [{ label = "swap";}];

  virtualisation = {

    virtualbox.host = {
      enable = true;
      enableHardening = false;
    };

    docker = {
      enable = true;
    };

  };

  users = {

    users.guest = {
      # Don't forget to set a password.
      name = "guest";
      group = "users";
      isNormalUser = true;
      uid = 1000;
      home = "/home/guest";
      createHome = true;
      initialPassword = "";
      extraGroups = [ "wheel" "disk" "audio" "video" "networkmanager" ];
    };

    users.lykrysh = {
      name = "lykrysh";
      group = "users";
      isNormalUser = true;
      uid = 1001;
      home = "/home/lykrysh";
      createHome = true;
      initialPassword = "";
      extraGroups = [ "wheel" "disk" "audio" "video" "networkmanager"  "vboxusers" "docker" ];
    };

    defaultUserShell = "/run/current-system/sw/bin/bash";

  };

  services = {

    ntp.enable = false;

    openssh.enable = true; # server & daemon all in it
    # sshd.enable = true; not necessary 

    printing.enable = true;

    xserver.enable = true;
    xserver.layout = "us";
    xserver.xkbOptions = "eurosign:e";
    xserver.libinput.enable = true; # touchpad
    xserver.desktopManager = {
      xfce.enable = true;
      default = "xfce";
    };

    postgresql.enable = true;
    postgresql.package = pkgs.postgresql94;
    postgresql.authentication = ''
      local all all               trust
      host  all all 127.0.0.1/32  trust
      host  all all ::1/128       trust
    '';

    cron.systemCronJobs = [];

  };

  system.stateVersion = "18.03"; 

}

</code></pre>
