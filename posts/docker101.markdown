---
title: Docker 101
published: 2018-05-01
author: Woodstock
tags: container
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

[Don't just `install` package](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-16-04)<br>
[Great hygiene advice](https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430)<br>
[Namespace](https://docs.docker.com/engine/security/userns-remap/)<br>
[Bridged networking](https://docs.docker.com/v17.09/engine/userguide/networking/)<br>
<br>
[Firewall](https://docs.docker.com/network/iptables/#restrict-connections-to-the-docker-daemon)<br>

<pre><code>
iptables -I DOCKER-USER -i ext_if ! -s <span> tun0.X.X.X/X </span> -j DROP

</code></pre>

<!-- more -->

## To fix X11 error

On host:

<pre><code>
# export DISPLAY=:0.0 -- already added in the run code
xhost +local:docker

</code></pre>
