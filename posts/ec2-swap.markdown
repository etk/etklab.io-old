---
title: Swap in macro
published: 2018-03-25
author: Woodstock
tags: aws
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

<pre><code>
sudo mkdir -v /var/cache/swap
cd /var/cache/swap
sudo dd if=/dev/zero of=swapfile bs=1K count=4M (this takes a while)
sudo chmod 600 swapfile
sudo mkswap swapfile
sudo swapon swapfile

# do things ...

cd /var/cache/swap
sudo swapoff swapfile

</code></pre>

<!-- more -->

https://medium.com/@saurabhnanda/building-haskell-specifically-lens-library-in-a-low-memory-environment-5ef58ec294a2