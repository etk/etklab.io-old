---
title: Switch native GCC on Gentoo
published: 2017-08-17
author: Woodstock
tags: gentoo
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Possible to have multiple GCC versions on root and easily switch them. (but why?)

<pre><code>
emerge =sys-devel/gcc-X.X.X
gcc-config -l
gcc-config ?
env-update && source /etc/profile 

</code></pre>

<!-- more -->



