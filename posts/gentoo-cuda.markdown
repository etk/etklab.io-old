---
title: Setup CUDA environment on Gentoo
published: 2017-08-17
author: Woodstock
tags: gentoo, gpu
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

<!-- more -->

Get gear ready as follows:

[List](https://developer.nvidia.com/cuda-gpus) of gpu cards for cuda<br>
Helpful videos [1](https://www.youtube.com/watch?v=PKJSRE6zzFY) and [2](https://www.youtube.com/watch?v=Qs_13V2OMLI) <br>
[Power supply](https://www.amazon.ca/Cx500-500w-80plus-Bronze-Psu/dp/B0092ML0MY)

Make sure uvm is used (it's for nvidia-cuda-toolkit).<br>
[Use tags](https://packages.gentoo.org/packages/dev-util/nvidia-cuda-toolkit) for nvidia-cuda-toolkit.<br>
[Use tags](https://packages.gentoo.org/packages/dev-util/nvidia-cuda-sdk) for the sdk.

<pre><code>
cat /etc/portage/package.use/nvidia-drivers
echo "x11-drivers/nvidia-drivers uvm" > /etc/portage/package.use/nvidia-drivers

// examples using profiler and opencl:
echo "dev-util/nvidia-cuda-toolkit profiler -eclipse -doc" > 
    /etc/portage/package.use/nvidia-cuda-toolkit
echo "dev-util/nvidia-cuda-sdk opencl -doc -examples -debug -mpi" > 
    /etc/portage/package.use/nvidia-cuda-sdk

</code></pre>

Before emerging nvidia-cuda:<br>
nvidia-cuda will require some mpi library installed.<br>
I prefer mpich to openmpi which seems harder to install on Gentoo. So I forced to use mpich by setting up lower openmpi version than cuda can possibly use:

<pre><code>
echo "sys-cluster/openmpi-1.4.5" > /etc/portage/package.mask/openmpi
emerge --ask --verbose dev-util/nvidia-cuda-toolkit
emerge --ask --verbose dev-util/nvidia-cuda-sdk

</code></pre>

Check CUDA with [Clang](http://llvm.org/docs/CompileCudaWithLLVM.html).<br>
More [cuda details](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html).
