---
title: Build GDB and Valgrind on Gentoo
published: 2017-08-15
author: Woodstock
tags: gentoo, debugging
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

On Gentoo, [glibc is stripped](https://wiki.gentoo.org/wiki/Debugging).<br>
Should edit `/etc/portage/make.conf`, `/etc/portage/env/debugsyms`, `/etc/portage/env/installsources` as documented.

<!-- more -->

<pre><code>
emerge --ask debugedit gdb

</code></pre>

Valgrind needs the debug symbols activated:<br>
add `FEATURES="splitdebug"` in `/etc/make.conf`.

<pre><code>
sys-libs/glibc debugsyms installsources >> /etc/portage/package.env
emerge --ask --oneshot sys-libs/glibc
// Update /etc/locale-gen
locale-gen
locale -a
emerge --ask valgrind

</code></pre>

