---
title: Gentoo Linux Installation (1)
published: 2017-08-12
author: Woodstock
tags: gentoo
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Last tested in 2017 Summer

<!-- more -->

## Install the kernel

Run liveCd. Open Konsole.

<pre><code>
sudo su -
passwd root

</code></pre>

Partition disk using GParted, found in the menu. <br>
Use the root passwd to login. Format disk. Mount partitions.

<pre><code>
/dev/sda1 [ ] 2M, bios_grub
/dev/sda2 [ext2] 250M /boot
/dev/sda3 [swap] 500M++
/dev/sda4 [ext4] rootfs /
/dev/sda5 [ext4] homefs will be /home/

mkfs.ext2 /dev/sda1
mkfs.ext2 /dev/sda2
mkfs.ext4 /dev/sda4
mkfs.ext4 /dev/sda5
mkswap /dev/sda3
swapon /dev/sda3

mount /dev/sda2 /mnt/gentoo
mkdir /mnt/gentoo/boot
mount /dev/sda1 /mnt/gentoo/boot

</code></pre>

Check lsblk. Get a [Stage3](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Stage) <br>
Copy DNS. Mount fs. Change root. Get Portage.
_Stay as (chroot) ‘till the end of installation!_

<pre><code>
cd /mnt/gentoo
wget http://distfiles.gentoo.org/...stage3...tar.bz2
tar xvjpf stage3...tar.bz2

cp -L /etc/resolv.conf /mnt/gentoo/etc/
mount -t proc none /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
source /etc/profile && export PS1="(chroot) $PS1"

mkdir /usr/portage
emerge-webrsync
emerge --sync
emerge vim

eselect profile list
eselect profile set // 3 to use Xfce with openRC

</code></pre>

In `/etc/portage/make.conf` (read [more about cflags](https://wiki.gentoo.org/wiki/GCC_optimization)): 

<pre><code>
CFLAGS=”-march=native …“, MAKEOPTS=”-jN” // where N is your #proc + 1

</code></pre>

## 2. Download the @world

<pre><code>
emerge --ask --update --deep --newuse @world // this takes a while

ls /usr/share/zoneinfo
echo "Europe/Brussels" > /etc/timezone
emerge --config sys-libs/timezone-data
vim /etc/locale.gen // en_US ISO-8859-1, en_US.UTF-8 UTF-8
locale-gen
vim /etc/conf.d/hostname
eselect locale list
eselect locale set 4 // choose en_US.utf8
env-update && source /etc/profile && export PS1="(chroot) $PS1"
vim /etc/env.d/02locale // LANG="en_GB.utf8", LC_COLLATE="C"

emerge --ask media-libs/libtxc_dxtn // for mesa
emerge --ask gentoo-sources
emerge --ask sys-apps/lshw // hardware specs
emerge --ask sys-apps/usbutils
emerge --ask sys-apps/pciutils
emerge --ask sys-kernel/linux-firmware

</code></pre>

Now we can open the kernel configuration window.

<pre><code>
cd /usr/src/linux
make menuconfig // just exit

emerge --ask genkernel
emerge --ask sys-fs/lvm2 // logical volume manager
emerge --ask sys-fs/mdadm // create RAID array
vim /etc/genkernel.conf // COMPRESS_INITRD="no"
genkernel --lvm --mdadm initramfs // initial ram-based file system

</code></pre>

In `/etc/lvm/lvm.conf	, use_lvmetad=1<br>
If buggy, in `/etc/conf.d/lvm`, RC_LVM_PROVIDE="lvmetad"<br>
In `/etc/portage/make.conf`, USE="udev ..."

<pre><code>
rc-update add lvm boot
rc-update add mdadm boot
rc-update add mdraid boot
rc-update add udev boot

emerge --depclean
emerge --ask openrc

eselect kernel list
eselect kernel set N
ln -sf /usr/src/linux-x.x.x-gentoo /usr/src/linux 
// Want to place the kernel in /usr/src/
ls -l /usr/src/linux
cd /usr/src/linux

</code></pre>

In `/etc/genkernel.conf`:<br>
MENUCONFIG="yes", <br>
MOUNTBOOT="yes", <br>
SAVE_CONFIG="yes", <br>
SYMLINK="yes", <br>
MAKEOPTS="-jN", <br>
LVM="yes", <br>
MDADM="yes"

<pre><code>
emerge --ask --oneshot --changed-use sys-apps/kmod // re-emerge kmod
depmod -a
modprobe 
genkernel --udev --lvm --mdadm –-menuconfig all // build the kernel

In /etc/portage/package.use: sys-apps/kmod lzma zlib

</code></pre>

To configure the kernel, consider: <br>
[this](https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation/Kernel), 
[this](https://wiki.gentoo.org/wiki/Kernel/Gentoo_Kernel_Configuration_Guide), 
[wifi](https://wiki.gentoo.org/wiki/Wifi), 
[Xorg](https://wiki.gentoo.org/wiki/Xorg/Guide), 
[lvm](https://wiki.gentoo.org/wiki/LVM#Software), 
[udev](https://wiki.gentoo.org/wiki/Udev), 
[hwclock](https://wiki.gentoo.org/wiki/System_time).<br>
Save the configuration at `/usr/src/linux/.myconfig`<br>
so later call back with the command `genkernel --kernel-config=/usr/src/linux/.myconfig --menuconfig all`

<pre><code>
ln -sf /proc/self/mounts /etc/mtab
ls /boot // check if the kernel is built
vim /etc/fstab 
// /dev/sda1 /boot ext2 noauto,noatime 1 2
// /dev/sda2 / ext4 defaults,noatime 0 1
// /dev/sda3 none swap sw 0 0

</code></pre>

**Important:** don't forget to create root passwd.

<pre><code>
passwd root
emerge sys-boot/grub

</code></pre>

In `/etc/default/grub`:<br>
GRUB_CMDLINE_LINUX="rootfstype=ext4 dolvm domdadm ...", <br>
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"<br>
Configure bootloader. <br>
Unmount mount points

<pre><code>
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

exit
cd /mnt
umount -R gentoo
reboot

</code></pre>

**Result:** reboot in TTY and login as root.

## 3. Get online

Run liveCd. Mount back to previous kernel points. <br>
Login as root.<br>
Install dhcp, 
[dhcpcd](https://wiki.gentoo.org/wiki/Network_management_using_DHCPCD) and [NetworkManager](https://wiki.gentoo.org/wiki/NetworkManager).

<pre><code>
emerge --ask net-misc/netifrc // creates /etc/init.d/net.lo
emerge --ask net-misc/dhcp
emerge --ask net-misc/dhcpcd
emerge --ask dhcpcd-ui
emerge --ask net-misc/networkmanager // wpa_supplicant included

ln -s /etc/init.d/net.lo /etc/init.d/net.eth0
cp /usr/share/dhcpcd/hooks/10-wpa_supplicant /lib/dhcpcd/dhcpcd-hooks

</code></pre>

In `/etc/wpa_supplicant/wpa_supplicant.conf`:

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel<br>
update_config=1 

In `/etc/conf.d/net`:

modules_eth0="wpa_supplicant"<br>
config_eth0="dhcp"<br>
wap_supplicant_eth0="-Dnl80211"

<pre><code>
chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf

rc-update add dbus boot
rc-update del net.eth0

echo 'sys-apps/openrc -netifrc' >> /etc/portage/package.use
emerge --ask --newuse --oneshot openrc
emerge --ask --depclean netifrc
rm /etc/conf.d/net
rm /etc/init.d/net.*

rc-update del dhcpcd
rc-update del wpa_supplicant
rc-update add NetworkManager default

rc-config list | grep "NetworkManager" // NetworkManager default
rc-config list | grep "net." // should be none. 
rc-config list | grep "dhcpcd" // dhcpcd
rc-config list | grep "wpa_supplicant" // wpa_supplicant

</code></pre>

Set group for networkmanager. Unmount.<br>
If networking works, then remove liveDc.

<pre><code>
getent group plugdev // if exists
gpasswd -a plugdev

exit
cd /mnt
umount -R gentoo
reboot

</code></pre>

**Result:** ping without liveCd.

## 4. Create a sudoers file

Check [Sudoers](http://www.sudo.ws/man/1.8.15/sudoers.man.html) later. The following is a minimal setting.

<pre><code>
emerge sudo
chown root:mail /var/spool/mail
chmod 03775 /var/spool/mail
echo "export EDITOR='/usr/bin/vim'" >> /etc/profile
source /etc/profile
visudo

</code></pre>

Uncomment `%wheel ALL=(ALL) ALL`<br>
Add a user.

<pre><code>
useradd 
    --create-home 
    --groups users,wheel,portage,plugdev,lp,video, cdrom, cdrw, usb 
    --shell /bin/bash 
    --comment "USERNAME" USERNAME
passwd USERNAME
getent group | grep USERNAME

</code></pre>

**Result:** do `sudo emerge ...`.
