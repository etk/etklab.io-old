---
title: Gentoo Linux Installation (2)
published: 2017-08-13
author: Woodstock
tags: gentoo
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Add graphical user interface. Xfce (lighter) or Gnome/KDE

<!-- more -->

## 5A. Build Xorg + Xfce

Check Wiki on [Xorg](https://wiki.gentoo.org/wiki/Xorg/Guide) and [xfce](https://wiki.gentoo.org/wiki/Xfce/Guide).<br>
In `/etc/portage/make.conf`:<br>
INPUT_DEVICES="evdev synaptics", VIDEO_CARDS="nouveau nvidia"

<pre><code>
eselect profile set [3] // desktop
emerge --ask --verbose x11-base/xorg-drivers
env-update && source /etc/profile
getent group | grep USERNAME // if not
gpasswd -a USERNAME video

// Test X
sudo emerge twm
sudo emerge xterm
startx

emerge --ask xfce-base/xfce4-meta
emerge --ask x11-terms/xfce4-terminal
emerge --ask x11-themes/xfwm4-themes
emerge --ask app-editors/mousepad
// more xfce4 goodies...

gpasswd -a cdrom cdrw usb
echo "exec startxfce4" > ~/.xinitrc

rc-update add xdm default
echo "x11-misc/slim branding" > /etc/portage/package.use/slim
emerge --ask x11-misc/slim

</code></pre>

In `/etc/conf.d/xdm`: DISPLAYMANAGER="slim"

<pre><code>
echo XSESSION=\"Xfce4\" > /etc/env.d/90xsession 
env-update && source /etc/profile

</code></pre>

Done :)


## 5B. Build Systemd + Gnome

This is an alternative to 5A.

<pre><code>
mount /boot
grub2-mkconfig -o /boot/grub/grub.cfg

</code></pre>

In `/etc/default/grub`:<br>
GRUB_CMDLINE_LINUX="... init=/usr/lib/systemd/systemd"


<pre><code>
umount /boot
ln -sf /proc/self/mounts /etc/mtab
eselect profile list // 5
emerge -avDN @world
// networking gone :-O
// check /etc/fstab ?

echo "US/Pacific" > /etc/timezone
emerge --config sys-libs/timezone-data
date // to check

</code></pre>

In `/etc/locale.gen`: en_US ISO-8859-1, en_US.UTF-8 UTF-8<br>
Check [systemd wiki](https://wiki.gentoo.org/wiki/Systemd).<br>
Localize env. In `/etc/locale.conf`: LANG="en_US.utf8", LC_COLLATE="C"<br>
[Synchronize hwclock](https://wiki.gentoo.org/wiki/System_time).

<pre><code>
locale-gen
env-update && source /etc/profile
systemd-machine-id-setup
hostnamectl set-hostname 
localectl set-locale LANG="en_US.utf8"
localectl set-keymap "en"
timedatectl set-ntp 1

</code></pre>

To get networking back working,<br>
In `/etc/systemd/network/50-dhcp.network`:

[Match]<br>
Name=en*

[Network]<br>
DHCP=yes


<pre><code>
systemctl enable systemd-networkd.service
systemctl start systemd-networkd.service
ping -s 5 google.ca
systemctl stop systemd-networkd.service

ln -snf /run/systemd/resolve/resolv.conf /etc/resolv.conf
systemctl enable systemd-resolved.service
systemctl start systemd-resolved.service
ping -s 5 google.ca

</code></pre>

In `/etc/portage/make.conf`: USE="-qt4 -kde X dbus gtk gnome ..."<br>
Install [X](https://www.x.org/wiki/) with [Xorg](https://wiki.gentoo.org/wiki/Xorg/Guide) server. Then Gnome.

<pre><code>
emerge --ask --verbose x11-base/xorg-server
emerge --ask x11-apps/xinit
nvidia-xconfig
env-update && source /etc/profile 

// this user should be in group 'video'
getent group | grep USERNAME // if not
gpasswd -a USERNAME video
emerge --ask gnome-base/gnome (see note** below)

</code></pre>

<hr>

**Note**:**<br>
When emerging gnome, There is possibly an old bug: openssh-openssl conflict on bindist<br>
Resolve like this:

<pre><code>
echo "net-misc/openssh -bindist" > /etc/portage/package.use/openssh
emerge --ask --newuse openssh

</code></pre>

Then after Gnome is installed, openssh and openssl can be rebuilt with bindist.

<pre><code>
rm /etc/portage/package.use/openssh
emerge --depclean empathy // this was the culprit
emerge --unmerge empathy
emerge --ask --newuse openssh

</code></pre>
<hr>

Follow [Gnome guide](https://wiki.gentoo.org/wiki/GNOME/Guide).

<pre><code>
env-update && source /etc/profile
/etc/init.d/dbus start 
rc-update add dbus default

</code></pre>

Login as a non-root user.

<pre><code>
echo "exec gnome-session" > ~/.xinitrc
sed -i '1i\export XDG_MENU_PREFIX=gnome-' ~/.xinitrc

sudo emerge twm
sudo emerge xterm
startx // see windows
sudo emerge --unmerge twm
sudo emerge --unmerge xterm
// Check getent group plugdev // if exists
gpasswd -a plugdev

</code></pre>

Use [GDM](https://wiki.gentoo.org/wiki/GNOME/gdm) to boot into Gnome.

<pre><code>
emerge --ask gnome-base/gdm
systemctl enable gdm.service
systemctl start gdm.service

lsmod | grep nvidia
lsmod | grep nouveau

</code></pre>
