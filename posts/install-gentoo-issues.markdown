---
title: Gentoo Linux Installation - resolve issues
published: 2017-08-14
author: Woodstock
tags: gentoo
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Grub issues | Disk partition issues | Kernel issues

<!-- more -->

## Grub issues

Reconfigure bootloader and boot params.<br>
[grub manual](http://www.gnu.org/software/grub/manual/grub.html)

<pre><code>
mount /dev/sda1 /boot
mount /dev/sda2 /

</code></pre>

Try `lsblk`. Check `/etc/default/grub`.

<pre><code>
grub-install /dev/sda
vim etc/default/grub // see installation notes for params
grub-mkconfig -o /boot/grub/grub.cfg
umount /boot
umount /
reboot

</code></pre>

## Disk partition issues,<br>or when kernel doesn't boot

Run liveCd. Open Konsole and mount back to the points.

<pre><code>
sudo su -
mount /dev/sda4 /mnt/gentoo
mount /dev/sda5 /mnt/gentoo/home
mount /dev/sda2 /mnt/gentoo/boot
mkswap /dev/sda3
swapon /dev/sda3
rm /mnt/gentoo/etc/resolv.conf
cp -L /etc/resolv.conf /mnt/gentoo/etc/
mount -t proc none /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) $PS1"
...

</code></pre>

Optional: make a backup by mounting a USB device.

<pre><code>
fdisk -l // find partition like /dev/sdb1
mkdir /media/my_mount_point
mount /dev/sdb1 /media/my_mount_point

// when done
umount /media/my_mount_point

</code></pre>

Do fixes.

<pre><code>
exit
cd /mnt
umount -R gentoo
reboot

</code></pre>

## Kernel issues,<br>or whenever needed to be reconfigured

No liveCd needed.<br>
Check /etc/fstab and fix if needed.<br>
It should look like /dev/sda1... /dev/sda2... /dev/sda3...

<pre><code>
mount /boot
genkernel 
    --kernel-config=/usr/src/linux/.myconfig 
    --udev --lvm --mdadm --menuconfig all 
emerge @module-rebuild

</code></pre>

Check /boot updated. Don't forget to **create root passwd**!

<pre><code>
rm /boot/*.old
passwd root

</code></pre>

Make sure `/etc/default/grub` is correctly set up: <br>
GRUB_CMDLINE_LINUX="...", GRUB_CMDLINE_LINUX_DEFAULT="..."<br>
Then configure bootloader.

<pre><code>
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

umount /boot
reboot

</code></pre>
