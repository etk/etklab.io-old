---
title: Siltaz Linux Environment
published: 2016-12-07
author: Julius
tags: other linux
description: Suspendisse pharetra ullamcorper sem et auctor. 
---

to do after vanilla installation

<!-- more -->

<pre><code>
tazpkg get-install slitaz-toolchain
// binutils linux-headers glibc-dev gcc make
tazpkg get-install slitaz-dev-pkgs
tazpkg get-install ncurses-dev perl

tazpkg get-install linux-source
tazpkg get-install linux-module-headers
tazpkg get-install module-init-tools

To activate .ssh:
add 'dropbear' for RUN_DAEMONS in the /etc/rcS.conf

tazpkg get-install sudo
visudo
// getent group 

</code></pre>