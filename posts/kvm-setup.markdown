---
title: KVM setup
published: 2018-05-17
author: Peppermint Patty
tags: kvm, other linux
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

Ubuntu:

<pre><code>
apt-get install -y qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils
usermod -aG libvirtd USER
usermod -aG kvm USER
apt-get install -y  virt-manager // optional

</code></pre> 

Fedora:

<pre><code>
dnf -y install qemu-kvm libvirt virt-install bridge-utils
usermod -aG libvirt USER
usermod -aG kvm USER
dnf -y install virt-manager // optional

</code></pre> 

<!-- more -->

To check:

<pre><code>
lsmod | grep kvm
	kvm_intel             192512  0
	kvm                   598016  1 kvm_intel
	irqbypass              16384  1 kvm
systemctl status libvirt // or libvirtd

</code></pre> 

Image types: qcow, qcow2, vmdk, vdi, vpc, vhdx, cloop, raw<br>
They go to `/var/lib/libvirt/images` by default<br>
<br>

_Reference:_<br>
[ubuntu help](https://help.ubuntu.com/community/KVM/Installation)<br>
[Awesome Fedora example](https://www.server-world.info/en/note?os=Fedora_28&p=kvm&f=1)<br>
[where to get qcow2 img](https://docs.openstack.org/image-guide/obtain-images.html)<br>
<br>
_To do next:_<br>
Bridged networking on commandline<br>
Manual `virt-install`
