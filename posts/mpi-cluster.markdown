---
title: Build a simple Beowulf MPI cluster
published: 2017-02-02
author: Woodstock
tags: protocols
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

[This](http://mpitutorial.com/tutorials/running-an-mpi-cluster-within-a-lan/) tutorial is super.<br>
First, it should be ssh-able each other machines (with password).<br>

<!-- more -->

Install openssh-server [mpich](http://www.mpich.org/).

#### SSH

Make sure `ssh username@host` works.

<pre><code>
vim /etc/ssh/sshd_config // change port of needed
rc-service sshd restart
rc-update add sshd default

</code></pre>

It's recommended to have a common username, say `mpiuser`

<pre><code>
useradd mpiuser
su - mpiuser

</code></pre>

#### SSH without PW

Get remote host/ip addresses

<pre><code>
ifconfig

</code></pre>

(Optional) make shortcuts of remote hosts.<br>
In /etc/hosts:
    127.0.0.1       localhost
    192.168.1.xx    client1
    192.168.1.xx    client2 ...

`ssh-agent` = pass-phrase cashing into mem.<br>
`ssh-add` = adding *identity* (private key) into ssh-agent.<br>
`ssh-copy-id` = appending the keys to the remote-host(client)’s .ssh/authorized_key.

<pre><code>
ssh-keygen -t rsa // i_am_server_rsa
eval `ssh-agent -s`
ssh-add ~/.ssh/i_am_server_rsa
ssh-copy-id -i ~/.ssh/i_am_server_rsa.pub client1
ssh-copy-id -i ~/.ssh/i_am_server_rsa.pub client2 ...

</code></pre>

Check `ssh client` (no pw prompt).

#### MPICH

should be all the same version if not shared.<br>
Check

<pre><code>
mpiexec --version

</code></pre>

Note: Need to re-build Boost MPI library.