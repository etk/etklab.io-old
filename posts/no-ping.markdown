---
title: Disable/enable the ping response
published: 2018-05-03
author: Peppermint Patty
tags: protocols
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

<pre><code>
echo 1 >/proc/sys/net/ipv4/icmp_echo_ignore_all # disable temporarily
echo 0 >/proc/sys/net/ipv4/icmp_echo_ignore_all # enable back
 
</code></pre> 

To disable permanently, in `/etc/sysctl.conf`:
<pre><code>
net.ipv4.conf.icmp_echo_ignore_all = 1
 
</code></pre> 

<!-- more -->

_Reference:_<br>
http://daddy-linux.blogspot.com/2012/02/disable-ping-response-on-linux.html

