---
title: Setup PostgreSQL
published: 2018-03-11
author: Woodstock
tags: sql, gentoo, nixos, aws
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

Gentoo | Nixos | EC2 Ubuntu Server

<!-- more -->

#### Gentoo

Root:

<pre><code>
emerge --config dev-db/postgresql:9.3
rc-update add postgresql-9.4 default
/etc/init.d/postgresql-9.4 stop
exit

</code></pre>

Non-root: 

<pre><code>
sudo vim /etc/postgresql-9.4/pg_hba.conf

</code></pre>

Change 'trust' to 'password' for the 'host' (not the 'local', Unix domain socket) connections.

<pre><code>
sudo /etc/init.d/postgresql-9.4 start
psql -U postgres
\password
...
\q

</code></pre>

<pre><code>
sudo vim /etc/postgresql-9.4/pg_hba.conf

</code></pre>

Change 'trust' to 'password' for the local connection:

<pre><code>
sudo /etc/init.d/postgresql-9.4 reload
psql -U postgres

</code></pre>

This should be OK


#### Nixos

In `configuration.nix`

<pre><code>
services.postgresql.enable = true;
services.postgresql.package = pkgs.postgresql94;
services.postgresql.authentication = ''
  local all all               trust
  host  all all 127.0.0.1/32  trust
  host  all all ::1/128
  '';

</code></pre>

#### EC2 Ubuntu Server

<pre><code>
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
sudo vim /etc/postgresql/9.4/main/pg_hba.conf

</code></pre>

Change 'trust' to 'md5' for the 'host' (not the 'local', Unix domain socket) connections.

<pre><code>
local   all             all                                     trust
# IPv4 local connections:
host    all             power_user      0.0.0.0/0               md5
host    all             other_user      0.0.0.0/0               md5
host    all             storageLoader   0.0.0.0/0               md5
# IPv6 local connections:
host    all             all             ::1/128                 md5

</code></pre>

<pre><code>
sudo vim /etc/postgresql/9.1/main/postgresql.conf

</code></pre>

<pre><code>
#listen_addresses = 'localhost'  
listen_addresses='*' 
port = 5432

</code></pre>

<pre><code>
sudo service postgresql start
sudo su - postgres
psql
  \password

</code></pre>

## test

<pre><code>
postgres=# CREATE ROLE testuser WITH LOGIN;
postgres=# \password testuser
postgres=# CREATE DATABASE testdb WITH OWNER testuser; -- username has all privileges on testdb
\q

psql -U testuser -d testdb
testdb=> create schema mySchema;
testdb=> \dn

</code></pre>

