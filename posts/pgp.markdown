---
title: PGP - quick how-to
published: 2017-11-04
author: Snoopy
tags: encryption
description: whatever
---

<!-- more -->

Install [gnupg](https://www.gnupg.org/download/)<br>
Generate my keypair

<pre><code>
gpg2 --full-gen-key

</code></pre>

Select: 1 (RSA and RSA) >> 4096 (bits long) >> 0 (valid forever)<br>
Give USERNAME >> email addr >> pw<br>
Now the keypair is at `~/.gnupg`

Check the keyID and fingerprint:

<pre><code>
gpg --fingerprint 

</code></pre>

Need the ascii representation of both keys:

<pre><code>
gpg --export -a USERNAME > ~/.gnupg/my_public.key
gpg --export-secret-key -a USERNAME > ~/.gnupg/my_private.key

</code></pre>

Place these files somewhere safe -- together with the matching USERNAME/pw/fingerprint<br>

It's good idea to upload `my_public.key` on the web (say, https://tinyurl.com/my_public.key )<br>
So I am ready to exchange files with my coworker Alice.

## Alive -> me

#### 1. Alice imports my pub key and _encrypts_ her file with it

Assume Alice knows where to find `my_public.key` and USERNAME

<pre><code>
curl -s https://tinyurl.com/my_public.key | gpg --import
gpg --recipient USERNAME --encrypt HER_FILE

</code></pre>

Then she will send `HER_FILE.gpg` to me.

#### 2. I receive the gpg-ed file and _decrypt_ it with my private key

<pre><code>
gpg HER_FILE.gpg

</code></pre>

## me -> Alice

#### 1. I _sign_ a file with my private key

<pre><code> 
gpg --sign <my_file>

</code></pre>

And send the gpg-ed file to Alice.

#### 2. Alice receives the gpg-ed file and _verify_ it with my pub key

<pre><code>
curl -s https://tinyurl.com/y7slvq5d | gpg --import
gpg MY_FILE.gpg

</code></pre>

<br>
<hr>
_References:_<br>
http://irtfweb.ifa.hawaii.edu/~lockhart/gpg/<br>
http://aperiodic.net/phil/pgp/tutorial.html<br>
https://help.github.com/articles/generating-a-new-gpg-key/<br>
