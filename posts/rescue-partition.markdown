---
title: Rescue to USB media
published: 2017-08-13
tags: bash
description: Suspendisse pharetra ullamcorper sem et auctor. Suspendisse vitae tellus eu.
---

When I have to rescue a full partition data

<pre><code>
fdisk -l // find partition eg. /dev/sdb1
mkdir /media/my_mount_point
mount /dev/sdb1 /media/my_mount_point

// when done
umount /media/my_mount_point

</code></pre>

<!-- more -->
