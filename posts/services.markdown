---
title: OpenRC vs. Systemd
published: 2018-04-01
author: Peppermint Patty
tags: gentoo, nixos, other linux
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

<table class="tiletable">
 <tr>
  <td>rc-update add/del [service] [default/runlevel]</td>
  <td class="gray">systemctl enable/disable [service]</td>
 </tr>
 <tr>
  <td class="gray">rc-update -v show<br>ls /etc/init.d</td>
  <td>systemctl list-unit-files -\-type=service<br>service -\-status-all</td>
 </tr>
 <tr>
  <td>rc-service [service] status</td>
  <td class="gray">systemctl status [service]</td>
 </tr>
 <tr>
  <td class="gray"></td>
  <td>systemctl is-enabled [service]</td>
 </tr>

</table>


<!-- more -->


<table class="tiletable">
 <tr>
  <td></td>
  <td class="gray">systemctl daemon-reload</td>
 </tr>
 <tr>
  <td class="gray"></td>
  <td>systemctl reload [service]</td>
 </tr>
 <tr>
  <td>rc-service [service] restart</td>
  <td class="gray">systemctl restart/condrestart [service]</td>
 </tr>
 <tr>
  <td class="gray">rc-service [service] start/stop</td>
  <td>systemctl start/stop [service]</td>
 </tr>
 <tr>
  <td>rc-service [service] zap</td>
  <td class="gray">systemctl kill [service]</td>
 </tr>
 <tr>
  <td class="gray"></td>
  <td>journalctl _SYSTEMD_UNIT=[service].service<br>// show log event</td>
 </tr>
</table>

_References_<br>
From Gentoo wiki: [OpenRC to Systemd](https://wiki.gentoo.org/wiki/OpenRC_to_systemd_Cheatsheet)<br>
From Fedora wiki: [Very nice Systemd summary](https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet)





