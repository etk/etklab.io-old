---
title: Source Directory?
published: 2016-11-13
tags: bash
description: Suspendisse pharetra ullamcorper sem et auctor. Suspendisse vitae tellus eu.
---

<pre><code>
# where this file is
SRC_DIR=$(cd $(dirname ${BASH_SOURCE[0]} ) && pwd)

# where this file is run
CUR_DIR=$(pwd)

</code></pre>

<!-- more -->
