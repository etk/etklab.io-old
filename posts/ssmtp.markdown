---
title: sSMTP
published: 2017-01-22
author: Charlie
tags: protocols
description: Suspendisse pharetra ullamcorper sem et auctor.
---

Immediate email access on the command line

<!-- more -->

In `/etc/ssmtp/ssmtp.conf`<br>

<pre><code>
root=myID@gmail.com
mailhub=smtp.gmail.com:587
rewriteDomain=
AuthUser=myID
AuthPass=myPW
FromLineOverride=YES
UseTLS=YES
UseSTARTTLS=YES

</code></pre>

In `/etc/ssmtp/revaliases`<br>

<pre><code>
root:myID@gmail.com:smtp.gmail.com:587
me_as_user:myID@gmail.com:smtp.gmail.com:587

</code></pre>

1. User permission 

<pre><code>
usermod -aG ssmtp me_as_user (Gentoo)

usermod -aG mail me_as_user (Debian)
chgrp mail /etc/ssmtp/ssmtp.conf (Debian)

</code></pre>

2. Gmail-specific: allow less secure apps [here](https://myaccount.google.com/lesssecureapps)

## Quick test

<pre><code>
sudo ssmtp yourMail@gmail.com // finish with Ctl-D
echo content | mail -s 'hello' someone@email.com

</code></pre>