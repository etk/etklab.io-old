---
title: Create PostgreSQL DB & User Access
published: 2018-04-10
author: Woodstock
tags: sql
description: Suspendisse pharetra ullamcorper sem et auctor turpis dignissim gravida ut ut tortor.
---

<pre><code>
psql -U postgres -- This should be OK

postgres=# CREATE ROLE testuser WITH LOGIN;
postgres=# \password testuser
postgres=# CREATE DATABASE testdb WITH OWNER testuser; -- username has all privileges on testdb
postgres=#\q

psql -U testuser -d testdb
testdb=> create schema mySchema;
testdb=> \dn

</code></pre>

<!-- more -->

