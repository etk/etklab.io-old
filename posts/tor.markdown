---
title: Tor | Torsocks | Privoxy
published: 2018-03-29
author: Peppermint Patty
tags: protocols, gentoo, nixos, other linux
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

Run command line (curl, wget, netcat...) from anonymous IP<br>
Want to do things like this:

<pre><code>
wget -qO- http://ipecho.net/plain ; echo                // real IP :(

torsocks wget -qO- http://ipecho.net/plain ; echo       // anonymous IP :) 
torify wget -qO- http://ipecho.net/plain ; echo         // anonymous IP :) 

curl ipinfo.io/ip                                       // real IP :(
torsocks curl ipinfo.io/ip                              // anonymous IP :)
torify curl ipinfo.io/ip   

torsocks nc %h %p

</code></pre> 

<!-- more -->

Install tor torsocks privoxy

#### Gentoo

<pre><code>
emerge net-misc/tor 
emerge www-proxy/privoxy

forward-socks4a / localhost:9050 . >> /etc/privoxy/config 

cp /etc/tor/torrc.sample /etc/tor/torrc // or edit
User tor
PIDFile /var/run/tor/tor.pid
Log notice syslog
DataDirectory /var/lib/tor/data
ControlPort 9051
CookieAuthentication 1

rc-update add tor default
rc-update add privoxy default

</code></pre> 

#### Debian/Kali

<pre><code>
...

sudo usermod -a -G debian-tor ubuntu
sudo update-rc.d tor defaults
sudo update-rc.d privoxy defaults

</code></pre> 

To check if tor service is running,

<pre><code>
sudo service --status-all | grep tor
+

</code></pre> 

#### NixOS

In `configuration.nix`:

<pre><code>
environment.systemPackages = with pkgs; [
    ... tor torsocks tsocks privoxy
];

services.tor.enable = true;
services.tor.client.enable = true;

</code></pre> 

#### Firefox

Edit > Preferences<br>
Advanced > Network > Settings

<pre><code>
manual proxy configuration:
http proxy           port: 0
ssl proxy            port: 0
ftp proxy            port: 0
socks host 127.0.0.1 port: 9050
check SOCKS v4
No Proxy for: localhost, 127.0.0.1

</code></pre> 

about:config<br>

<pre><code>
network.proxy.socks_remote_dns    true
network.dns.disablePrefetch       true
network.dns.disableIPv6           true

media.peerconnection.enabled    false

</code></pre>

_Reference:_<br>
https://wiki.gentoo.org/wiki/Tor

#### Run python::torrequest from anonymous IP

<pre><code>
ControlPort 9051 >> /etc/tor/torrc
# test: run tw/00testtor.py

</code></pre> 
