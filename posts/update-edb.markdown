---
title: Re-install latest EDB (Evan's Debugger)
published: 2018-01-04
author: Snoopy
tags: debugging, other linux
description: Suspendisse pharetra ullamcorper magna ultricies congue in a sem et auctor. Suspendisse vitae tellus eu turpis dignissim gravida ut ut tortor.
---

on Debian distros like Kali

<!-- more -->

<pre><code>
apt-get purge edb-debugger
apt-get install qt-sdk
apt-get install libboost-dev
apt-get install libcapstone3 libcapstone-dev
apt-get install graphviz graphviz-dev

mkdir /path/to
git clone --recursive https://github.com/eteran/edb-debugger
cd edb-debugger
mkdir build
cd build
cmake ..
make && make install

</code></pre>

edb is now at `/usr/local/bin/edb`<br>
plugins at `/usr/local/lib/edb`
