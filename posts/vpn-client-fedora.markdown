---
title: OpenVPN Client Setup on Fedora
published: 2018-04-04
author: Peppermint Patty
tags: encryption, other linux
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

Root job only.<br>
Place `ca.crt` `client1.crt` `client1.key` `ta.key` in `/etc/openvpn/client/MyClient`<br>
The conf file is at `/etc/openvpn/client/MyClient.conf`<br>
 
<pre><code>
ln -s /lib/systemd/system/openvpn-client@.service /etc/systemd/system/openvpn-client@MyClient.service
systemctl enable openvpn-client@MyClient.service
systemctl start openvpn-client@MyClient.service

</code></pre>

<!-- more -->

_Reference:_<br>
https://fedoraproject.org/wiki/Openvpn
