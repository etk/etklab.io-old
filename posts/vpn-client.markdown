---
title: OpenVPN Client Setup
published: 2018-03-25
author: Peppermint Patty
tags: encryption
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

Get `ca.crt` `client1.crt` `client1.key`<br>
Chmod 400 `client1.key`<br>
In `/etc/openvpn/openvpn.conf`:

<pre><code>
client 
dev tun0 
proto tcp 
remote <span> where.server.is.located </span> <span> port </span>
remote-cert-tls server
cipher AES-256-CBC
comp-lzo 
resolv-retry infinite 
nobind 
persist-key 
persist-tun 
ca /where/is/ca.crt
cert /where/is/client1.crt 
key /where/is/client1.key 
script-security 2
auth-nocache
log /somewhere/openvpn.log
verb 1
pull

</code></pre>

<!-- more -->

## To harden security with tls-auth key

Get `ta.key` and chmod 400<br>
Add to `/etc/openvpn/openvpn.conf`:

<pre><code>
--tls-auth /my/ta.key 1

</code></pre> 

<h2>To test</h2>

Both on server and client, ifconfig then ping each other<br>
Look at the log files<br>
Check my ip from [website](https://www.dnsleaktest.com/)
<br><br>
To turn on/off `sudo openvpn --config /my/openvpn.conf`
