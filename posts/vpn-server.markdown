---
title: Minimal OpenVPN Server
published: 2018-03-24
author: Peppermint Patty
tags: aws, encryption
description: Suspendisse in aliquam in imperdiet et, sagittis eu ligula vulputate est eros ut tortor dolor placerat lobortis tempor, pharetra ullamcorper sem et.
---

In `/etc/openvpn/openvpn.conf`:

<pre><code>
port <span> port </span>
proto tcp
dev tun0
ca vpn/ca.crt
cert vpn/server.crt
key vpn/server.key
dh vpn/dh2048.pem
server <span> tun0.X.X.X </span> 255.255.255.0
ifconfig-pool-persist ipp.txt
cipher AES-256-CBC
comp-lzo
persist-key
persist-tun
verb 3
status openvpn-status.log
log /etc/openvpn/openvpn.log
push "redirect-gateway def1 bypass-dhcp"
keepalive 10 120

</code></pre> 

<!-- more -->

#### EC2 Instance

_(fresh) Security Group: _<br>
(SSH)			TCP	22  	Custom	0.0.0.0/0<br>
(Custom TCP Rule)		TCP	<span> port </span>  Anywhere	0.0.0.0/0, ::/0

_Instances:_<br>
Actions > Networking > Change Security Groups: add _the one just made_<br>
Actions > Networking > Change Source/Dest. Check: _Yes, Disable_

_Elastic IPs:_<br>
Allocate new address > Allocate, Close<br>
Actions > Associate address > Type: Instance, input the instance ID > Associate<br>

Also enable _Termination Protection_<br>
SSH to the console:

<pre><code>
sudo su -
apt-get update
apt-get upgrade
apt-get install openvpn easy-rsa

cd /usr/share/easy-rsa/
. ./vars  
./clean-all  
./build-ca  
./build-key-server server
./build-key client1
./build-dh

mkdir -p /etc/openvpn/vpn
cd keys
cp ca.crt dh2048.pem server.crt server.key /etc/openvpn/vpn
vim /etc/openvpn/openvpn.conf

</code></pre> 

Write `/etc/openvpn/openvpn.conf` as above<br>
NAT rule, the VPN client traffic to the internet:

<pre><code>
iptables -t nat -A POSTROUTING -s <span> tun0.X.X.X/X </span> -o eth0 -j MASQUERADE
echo 1 > /proc/sys/net/ipv4/ip_forward

</code></pre>

<pre><code>
systemctl enable openvpn.service
systemctl start openvpn.service
netstat -an # check the tcp port

</code></pre> 

Download(scp) `ca.crt`, `client1.crt`, `client1.key` from `/usr/share/easy-rsa/keys/`

## To harden security with tls-auth key

<pre><code>
cd /etc/openvpn/
openvpn --genkey --secret vpn/ta.key

</code></pre> 

Add to `/etc/openvpn/openvpn.conf`:

<pre><code>
--tls-auth vpn/ta.key 0

</code></pre> 

<h2>To revoke a client certificate</h2>

[Here](https://openvpn.net/index.php/open-source/documentation/howto.html#revoke)
