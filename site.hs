--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Data.List (intercalate)
import           Control.Monad
import           Text.Blaze.Html (toHtml, toValue, (!))
import           Text.Blaze.Html.Renderer.String (renderHtml)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import           System.FilePath (takeFileName)
import           Hakyll
import qualified GHC.IO.Encoding as E

--------------------------------------------------------------------------------
main :: IO ()
main = do 
  E.setLocaleEncoding E.utf8
  hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match (fromList ["404.md", "404.html"]) $ do
        route   idRoute
        compile copyFileCompiler

    match "js/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*.css" $ do
        route   idRoute
        compile compressCssCompiler
        {- compile copyFileCompiler -}

    match "css/*.ttf" $ do
        route   idRoute
        compile copyFileCompiler

    tags <- buildTags "posts/*" $ fromCapture "tags/*.html"

    tagsRules tags $ \tag pattern -> do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAllSnapshots pattern "teaser"
            let ctx =
                  constField "title" tag `mappend`
                  listField "posts" postCtx (return posts) `mappend`
                  defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/tag.html" ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ do
            compiled <- pandocCompiler
            teaser <- loadAndApplyTemplate "templates/post-teaser.html" defaultContext $ grabTeaser compiled
            saveSnapshot "teaser" teaser

            full <- loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags) compiled
            loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags) full
                >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    field "taglist" (\_ -> renderTagList' (sortTagsBy descendingTags tags)) `mappend`
                    listField "posts" postCtx (return posts) `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls

    paginate <- buildPaginateWith pagesGrouper "posts/*" makePageId

    paginateRules paginate $ \page pattern -> do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAllSnapshots pattern "teaser"
            let indexCtx =
                    field "taglist" (\_ -> renderTagList' (sortTagsBy descendingTags tags)) `mappend`
                    listField "posts" postCtx (return posts) `mappend`
                    paginateContext paginate page `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/index.html" indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext

--------------------------------------------------------------------------------
postCtxWithTags :: Tags -> Context String
postCtxWithTags tags =
    tagsField "tags" tags `mappend`
    postCtx

renderTagList' :: Tags -> Compiler (String)
renderTagList' =
    renderTags makeLink (intercalate ", ")
        where
          makeLink tag url count _ _ = renderHtml $
            H.a ! A.href (toValue url) $ toHtml (tag ++ " (" ++ show count ++ ")")

descendingTags :: (String, [Identifier]) -> (String, [Identifier]) -> Ordering
descendingTags x y =
    compare (length (snd y)) (length (snd x))

--------------------------------------------------------------------------------
grabTeaser :: Item String -> Item String
grabTeaser =
    fmap (unlines . takeWhile (/= "<!-- more -->") . lines)

--------------------------------------------------------------------------------
makePageId :: PageNumber -> Identifier
makePageId n =
    fromFilePath $
        case n of
           1 -> "index.html"
           _ -> show n ++ "/index.html"

pagesGrouper :: MonadMetadata m => [Identifier ] -> m [[Identifier]]
pagesGrouper = liftM (paginateEvery 7) . sortRecentFirst

